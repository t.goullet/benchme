# Benchme

TP 2 - Algorithmes de tri

Language = C

Auteurs : GOULLET Thibaut & Alexis Cartelier

## Objectif du TP

L'objectif de ce TP est d'implémenter quatres algorithmes de tri. Le tri à bulle, le tri par insertion, le tri par séléction et le tri par tas. Ces quatres algortihmes sont capables d'effectuer un tri croissant ou décroissant d'un tableau de nombre à virgules.
Il sera également possible de mesurer le temps d'exécution de chacun des algorithmes selon la taille du tableau.
Les tests effectués permettront de créer un graphique des temps d'exécution.

## Usage des commandes

benchme "nom du fichier" permet de créer un fichier csv où sera répertorier tout les temps d'exécution des algorithmes.

### Tri à bulle

Il consiste à comparer répétitivement les éléments consécutifs d'un tableau, et à les permuter lorsqu'ils sont mal triés. Il déplace rapidement les plus grands éléments en fin de tableau.

### Tri par insertion

Le tri par insertion considère chaque élément du tableau et l'insère à la bonne place parmi les éléments déjà triés. Ainsi, au moment où on considère un élément, les éléments qui le précèdent sont déjà triés, tandis que les éléments qui le suivent ne sont pas encore triés.

### Tri par séléction

Le tri par séléction s'exécute en trois étapes : 
- Recherche du plus petit élément du tableau et l'échanger avec le premier élément.
- Rechercher le second plus petit élément du tableau et l'échanger avec le second éléments.
- Et faire ainsi jusqu'à ce que le tableau soit entièrement trié.

L'algorithme du tri par tas repose sur un élément fondamental : le tas (d'où son nom). En effet, ce tri crée un tas max du tableau donné en entrée, et le parcourt afin de reconstituer les valeurs triées dans notre tableau.


## Les résultats attendus

Chacun des algorithmes est capable de trier un tableau de valeurs aléatoires comprises entre 0 et 10⁶. Pour chaque algorithmes, on peut obtenir le temps d'exécution de ce dernier et en faire la moyenne par rapport à plusieurs tests et nombres de valeurs dans le tableau. Grâce à cela, on pourra déterminer quel algorithme est le plus efficace pour trier un tableau de valeur.
