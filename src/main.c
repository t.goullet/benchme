#include <stdio.h>
#include <stdlib.h>
#include <time.h>
/* #include "../include/Tri_A_Bulle.h"
#include "../include/Tri_Par_Insertion.h"
#include "../include/tri_Par_Selection.h"
#include "../include/Tri_Par_Tas.h" */

int main()
{
	
	clock_t debut, fin; 
	float temp_bulle, temp_insert, temp_tas, temp_selec, temp_total[2];

	//initialisation des tableau et de leur tailles
	int taille;
	printf("Entrez la taille de votre tableau de valeurs aléatoires : ");
	scanf("%d", &taille);
	float tabtri[taille];
	float tab_global[3][taille];

	//initialisation de la seed random
	srand((unsigned int)time(NULL));

	//set des trois liste de valeur aleatoire dans un tableau global
	for (int i = 0; i < taille; i++)
	{
		int nb = rand() % 100 + 1;
		tab_global[0][i] = ((float)rand() / (float)(RAND_MAX)) + nb;
	}
	for (int j = 0; j < taille; j++)
	{
		int nb = rand() % 100 + 1;
		tab_global[1][j] = ((float)rand() / (float)(RAND_MAX)) + nb;
	}
	for (int k = 0; k < taille; k++)
	{
		int nb = rand() % 100 + 1;
		tab_global[2][k] = ((float)rand() / (float)(RAND_MAX)) + nb;
	}

	//test du Tri_A_Bulle avec trois tableau different
	for (int l = 0; l < 3; l++)
	{
		//transfere du l-eme tableau dans tabtri, le tableau qui vas etre trié
		for (int m = 0; m < taille; m++)
		{
			tabtri[m] = tab_global[l][m];
		}

		//lancement du compteur de temps
		debut = clock();

		//trie du tableau
		Tri_A_Bulle(tabtri, taille);

		//fin du compteur de temps
		fin = clock();

		//calcule et affichage du temp total en ms
		temp_total[l] = fin - debut;
		printf("tour %d : %.2f ms\n ", l, temp_total[l]);
	}
	//calcule et affichage de la moyene des different temps en ms pour Tri_A_Bulle
	temp_bulle = (temp_total[0] + temp_total[1] + temp_total[2]) / 3;
	printf("la moyenne de temps pour les differents testes de Tri_A_Bulle est de : %.2f ms\n", temp_bulle);

	for (int l = 0; l < 3; l++)
	{
		for (int m = 0; m < taille; m++)
		{
			tabtri[m] = tab_global[l][m];
		}

		debut = clock();

		tri_Par_Selection(tabtri, taille);

		fin = clock();

		temp_total[l] = fin - debut;
		printf("tour %d : %.2f ms\n ", l, temp_total[l]);
	}
	temp_selec = (temp_total[0] + temp_total[1] + temp_total[2]) / 3;
	printf("la moyenne de temps pour les differents testes de tri_Par_Selection est de : %.2f ms\n", temp_selec);

for (int l = 0; l < 3; l++)
	{
		for (int m = 0; m < taille; m++)
		{
			tabtri[m] = tab_global[l][m];
		}

		debut = clock();

		Tri_Par_Insertion(tabtri, taille);

		fin = clock();

		temp_total[l] = fin - debut;
		printf("tour %d : %.2f ms\n ", l, temp_total[l]);
	}
	//calcule et affichage de la moyene des different temps en ms pour Tri_Par_Insertion
	temp_insert = (temp_total[0] + temp_total[1] + temp_total[2]) / 3;
	printf("la moyenne de temps pour les differents testes de Tri_Par_Insertion est de : %.2f ms\n", temp_insert);

	//test de Tri_Par_Tas avec trois tableau different
	for (int l = 0; l < 3; l++)
	{
		for (int m = 0; m < taille; m++)
		{
			tabtri[m] = tab_global[l][m];
		}

		debut = clock();

		Tri_Par_Tas(tabtri, taille);

		fin = clock();

		temp_total[l] = fin - debut;
		printf("tour %d : %.2f ms\n ", l, temp_total[l]);
	}
	//calcule et affichage de la moyene des different temps en ms pour Tri_Par_Tas
	temp_tas = (temp_total[0] + temp_total[1] + temp_total[2]) / 3;
	printf("la moyenne de temps pour les differents testes de Tri_Par_Tas est de : %.2f ms\n", temp_tas);
}