#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void swap(int *a, int *b)
{
    int temp = *a;
    *a = *b;
    *b = temp;
}

void heapify(int tab[], int n, int i)
{
    int max = i; 
    int left = 2 * i + 1;
    int right = 2 * i + 2;

    if (left < n && tab[left] > tab[max])
        max = left;

    if (right < n && tab[right] > tab[max])
        max = right;

    if (max != i)
    {
        swap(&tab[i], &tab[max]);
        heapify(tab, n, max);
    }
}

void Tri_Par_Tas(float *tab, int longueur)
{

    for (int i = longueur / 2 - 1; i >= 0; i--)
        heapify(tab, longueur, i);

    for (int i = longueur - 1; i >= 0; i--)
    {
        swap(&tab[0], &tab[i]); 
        heapify(tab, i, 0); 
    }
}
